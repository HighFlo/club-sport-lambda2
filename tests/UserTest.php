<?php
    //ceci remplace l'instruction include quand on défini un namespace à la classe
    use PHPUnit\Framework\TestCase;

    include(__DIR__ ."/../models/user.php");
    include(__DIR__ ."/../models/seance.php");
    include(__DIR__ ."/../models/database.php");

    final class UserTest extends TestCase
    {
        public function testCreateUser()
        {
            $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));
            
            $database = new Database();

            $this->assertNotFalse($database->createUser($user));
        }

        public static function tearDownAfterClass()
        {
            $database = new Database();
            $database->deleteAllInscrits();
            $database->deleteAllUsers();
            $database->deleteAllSeances();
        }

        public function testGetAndActivateUser()
        {
            $database = new Database();

            //Créé le user
            $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));
                        
            //L'insérer et récupérer son id puis vérifier que tout s'est bien passé
            $id = $database->createUser($user);
            $this->assertNotFalse($id);

            //Activer le user
            $this->assertTrue($database->activateUser($id));

            //Récupérer le user via son id
            $user = $database->getUserById($id);
            $this->assertInstanceOf(User::class, $user);

            //Vérifier que le user est actif
            $this->assertEquals(1, $user->isActif());
        }

        public function testEmailAlreadyExists()
        {
            $database = new Database();

            //Créer le user
            $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));

            //L'insérer puis vérifier que tout s'est bien passé
            $this->assertNotFalse($database->createUser($user));
            
            //Vérifier un email qui existe
            $emailTrue = "toto@gmail.com";
            $this->assertTrue($database->isEmailExists($emailTrue));

            //Vérifier un email qui n'existe pas
            $emailFalse = "toto@hotmail.com";
            $this->assertFalse($database->isEmailExists($emailFalse));
        }

        public function testGetUserByEmail()
        {
            $database = new Database();

            //Créer le user
            $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));

            //L'insérer puis vérifier que tout s'est bien passé
            $this->assertNotFalse($database->createUser($user));

            //Vérifier qu'on récupère bien le user grâce à son email
            $emailTrue = "toto@gmail.com";
            $this->assertInstanceOf(User::class, $database->getUserByEmail($emailTrue));

            //Vérifier qu'on ne récupèere personne si l'email n'existe pas
            $emailFalse = "toto@hotmail.com";
            $this->assertFalse($database->getUserByEmail($emailFalse));
        }
    }
?>