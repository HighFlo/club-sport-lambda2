<?php
    //Ce fichier sert à activer un utilisateur qui vient de s'inscrire

    //On va utiliser la session pour passer des messages d'une page à l'autre
    //Pour cela il faut démarrer la session au début des pages concernées
    session_start();

    require_once(__DIR__ ."/../models/Database.php");
    $database = new Database();

    //On récupère les données dans l'url
    $idUser = isset($_GET["id"]) ? $_GET["id"] : null;
    $token = isset($_GET["token"]) ? $_GET["token"] : null;

    //On fait les vérifications d'usage
    if($idUser == null || $token == null)
    {
        $_SESSION["error"] = "Un problème est survenu lors de votre inscription, veuillez recommencer.";
        header("location: ../vues/inscription.php");
        exit();
    }

    //On cherche l'utilisateur dans la BD grâce à son id
    $user = $database->getUserById($idUser);

    //On vérifie que le user à bien été retrouvé
    if(!$user)
    {
        $_SESSION["error"] = "Un problème est survenu lors de votre inscription, vueillez recommencer.";
        header("location: ..vues/inscription.php");
        exit();
    }

    //On compare les tokens pour authentifier l'utilisateur
    if($token != $user->getToken())
    {
        $_SESSION["error"] = "Un problème est survenue lors de votre inscription, veuillez recommencer.";
        header("location: ../vues/inscription.php");
        exit();
    }

    //Si tout s'est bien passé on active l'utilisateur
    if($database->activateUser($idUser))
    {
        //Puis on redirige vers la page de login avec un message de succès
        $_SESSION["info"] = "Votre compte à été activé, vous pouvez vous connecter.";
        header("location: ../vues/login.php");
    }
    else
    {
        //Si l'activation à échoué on renvoie vers la page inscription
        $_SESSION["error"] = "Un problème est survenue lors de l'activation de votre compte, veuillez recommencer.";
        header("location: ../vues/inscription.php");
    }
?>