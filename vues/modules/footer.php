<footer>
    <div class = "container-fluid bg-info text-white pt-2">
        <div class = "row text-center text-xs-center text-sm-left text-md-left">
            <div class = "col-xs-12 col-sm-4 col-md-4">
                <h5>Quick Links</h5>
                <div class = "list-unstyled d-flex justify-content-around">
                    <a href = "#">Home</a>
                    <a href = "#">About</a>
                    <a href = "#">FAQ</a>
                    <a href = "#">Get Started</a>
                    <a href = "#">Videos</a>
                </div>
            </div>
        </div>
        <div class = "row">
            <div class = "col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center badge badge-dark">
                <p class = "h6">& copy All Rights Reserved. <a class = "ml-2" href = "https://www.realise.ch"
                target = "_blank">Realise</a><p>
            </div>
            </hr>
        </div>
    </div>
</footer>